package com.example.springbootproject.services;
import java.util.ArrayList;  
import java.util.List;  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.springbootproject.entity.etudiant;  
import com.example.springbootproject.repository.Repository;  

@Service
public class serviceetudiant {
	@Autowired  
	Repository etudiantRepository;  
	 
	public List<etudiant> getAlletudiant()   
	{  
	List<etudiant> etudiants = new ArrayList<etudiant>();  
	this.etudiantRepository.findAll().forEach(etudiant1 -> etudiants.add(etudiant1));  
	return etudiants;  
	}  
	
	public etudiant getetudiantBycne(int cne)   
	{  
	return etudiantRepository.findById(cne).get();  
	}  
	
	public void saveOrUpdate(etudiant e)   
	{  
		etudiantRepository.save(e);  
	}  
	
	public void delete(int cne)   
	{  
		etudiantRepository.deleteById(cne);  
	}  
	 
	public void update(etudiant e, int cne)   
	{  
		etudiantRepository.save(e);  
	}  

}
