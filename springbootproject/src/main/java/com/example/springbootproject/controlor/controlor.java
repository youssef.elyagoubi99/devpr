package com.example.springbootproject.controlor;
import java.util.List;  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;  
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.PostMapping;  
import org.springframework.web.bind.annotation.PutMapping;  
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;  
import com.example.springbootproject.entity.etudiant; 
import com.example.springbootproject.services.serviceetudiant; 

@Controller
@RequestMapping("/")  

public class controlor {
	

	
	@Autowired  
	serviceetudiant etudserv;  
	@GetMapping("/")
	public String index(Model m) {
		List<etudiant> e = etudserv.getAlletudiant();
		m.addAttribute("etudiantlist",e);
		return "index";
	}
	@GetMapping("/form")
	public String form(Model m) {
		etudiant e = new etudiant();
		m.addAttribute("etudiantlist",e);
		return "form";
	}
	
	@PostMapping("/save")
	public String save(@ModelAttribute("etudiant") etudiant e) {
		etudserv.saveOrUpdate(e);
		
		return "redirect:/";
	}
	
	@GetMapping("/edit")
	public String edit(@RequestParam("cne") int cne, Model m) {
		etudiant e = etudserv.getetudiantBycne(cne);
		m.addAttribute("etudiantlist",e);
		return "form";
	}
	@GetMapping("/delete")
	public String delete(@RequestParam("cne") int cne) {
		etudserv.delete(cne);
		return "redirect:/";
	}
	
	
	
	
	
	
	
	
	
	/*
	@GetMapping("/etudiant")  
	private List<etudiant> getAlletudiants()   
	{  
	return etudserv.getAlletudiant();  
	}  
	
	@GetMapping("/etudiant/{cne}")  
	private etudiant getetud(@PathVariable("cne") int cne)   
	{  
	return etudserv.getetudiantBycne(cne);  
	}  
	
	@DeleteMapping("/etudiant/{cne}")  
	private void deleteetud(@PathVariable("cne") int cne)   
	{  
		etudserv.delete(cne);  
	}  
	
	@PostMapping("/etudiant")  
	private int saveetudiant(@RequestBody etudiant e)   
	{  
		etudserv.saveOrUpdate(e);  
	return e.getCne();  
	}  
	 
	@PutMapping("/etudiant")  
	private etudiant update(@RequestBody etudiant e)   
	{  
		etudserv.saveOrUpdate(e);  
	return e;  
	}  
	*/
	}
