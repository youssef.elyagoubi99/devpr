package com.example.springbootproject.repository;  
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;  
import com.example.springbootproject.entity.etudiant;  




public interface Repository extends JpaRepository<etudiant, Integer>  
{  
}  