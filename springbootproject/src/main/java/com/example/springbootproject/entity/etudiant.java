package com.example.springbootproject.entity;

import javax.persistence.Column;  
import javax.persistence.Entity;  
import javax.persistence.Id;  
import javax.persistence.Table;  
//mark class as an Entity   
@Entity  
//defining class name as Table name  
@Table  
public class etudiant  
{  
//Defining book id as primary key  
@Id  
@Column  
private int cne;  
@Column  
private String nom;  
@Column  
private String prenom;
public int getCne() {
	return cne;
}
public void setCne(int cne) {
	this.cne = cne;
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public String getPrenom() {
	return prenom;
}
public void setPrenom(String prenom) {
	this.prenom = prenom;
}


}  